#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <cstring>

using namespace std;

int main() {
  size_t pagesize = getpagesize();
  long int addr = (pagesize * (1 << 20));
  
  char * region = (char*)mmap((void*) addr, pagesize, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_ANON|MAP_PRIVATE, 0, 0);
  if (region == MAP_FAILED) {
    perror("Could not mmap");
    return 1;
  }
  
  printf("Writing to %p\n", (void*)region);
  strcpy(region, "hmmm");
  
  while(1){};

  return 0;
}