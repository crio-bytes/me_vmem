/*
source: 
fivelinesofcode.blogspot.com/2014/03/how-to-translate-virtual-to-physical.html
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <iostream>
using namespace std;
#define PAGEMAP_ENTRY 8
#define GET_BIT(X,Y) (X & ((uint64_t)1<<Y)) >> Y
#define GET_PFN(X) X & 0x7FFFFFFFFFFFFF

const int __endian_bit = 1;
#define is_bigendian() ( (*(char*)&__endian_bit) == 0 )

int i, c, pid, status;
unsigned long virt_addr; 
uint64_t read_val, file_offset, virt_pfn;
char path_buf [0x100] = {};
FILE * f;
char *end;

int read_pagemap(char * path_buf, unsigned long virt_addr);

int main(int argc, char ** argv){
   if(argc!=3){
      printf("Usage: sudo ./a.out PID VIRTUAL_ADDRESS\n");
      return -1;
   }

   pid = strtol(argv[1],&end, 10);
       
      
   virt_addr = strtol(argv[2], NULL, 16);
   sprintf(path_buf, "/proc/%u/pagemap", pid);
   
   read_pagemap(path_buf, virt_addr);
   return 0;
}

int read_pagemap(char * path_buf, unsigned long virt_addr){
   f = fopen(path_buf, "rb");
   if(!f){
      printf("Error! Cannot open %s\n", path_buf);
      return -1;
   }
   
   //Shifting by virt-addr-offset number of bytes
   //and multiplying by the size of an address (the size of an entry in pagemap file)
   file_offset = virt_addr / getpagesize() * PAGEMAP_ENTRY;
   printf("VirtualPFN: 0x%lx, Page_size: %d, Entry_size: %d\n", virt_addr / getpagesize(), getpagesize(), PAGEMAP_ENTRY);
   status = fseek(f, file_offset, SEEK_SET);
   if(status){
      perror("Failed to do fseek!");
      return -1;
   }
   errno = 0;
   read_val = 0;
   unsigned char c_buf[PAGEMAP_ENTRY];
   for(i=0; i < PAGEMAP_ENTRY; i++){
      c = getc(f);
      if(c==EOF){
         printf("\nReached end of the file\n");
         return 0;
      }
      if(is_bigendian())
           c_buf[i] = c;
      else
           c_buf[PAGEMAP_ENTRY - i - 1] = c;
   }
   printf("Pagemap entry: ");
   for(i=0; i < PAGEMAP_ENTRY; i++){
      printf("%d ",c_buf[i]);
      read_val = (read_val << 8) + c_buf[i];
   }
   printf("\n");
   //printf("Result: 0x%llx\n", (unsigned long long) read_val);

   if(GET_BIT(read_val, 63)){
      printf("PFN: 0x%llx\n",(unsigned long long) GET_PFN(read_val));
   } else
      printf("Page not present\n");
   if(GET_BIT(read_val, 62))
      printf("Page swapped\n");
   fclose(f);
   return 0;
}