#include <stdio.h>
#include<stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <cstring>

using namespace std;

int main(int argc, char**argv) {
  if (argc != 2) {
	printf("Usage: ./a.out TextToWrite\n");
	return 0;
  }

  size_t pagesize = getpagesize();
  long int addr = (pagesize * (1 << 20));

  char * region = (char*)mmap((void*) addr, pagesize, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_ANON|MAP_PRIVATE, 0, 0);
  if (region == MAP_FAILED) {
    perror("Could not mmap");
    return 1;
  }

  printf("Writing '%s' to %p\n", argv[1], (void*)region);
  strcpy(region, argv[1]);

  char *read = (char*) addr;

  while(1) {
	printf("Hey, I found '%s' at %p\n", read, (void*)read);
	sleep(3);
  };

  return 0;
}